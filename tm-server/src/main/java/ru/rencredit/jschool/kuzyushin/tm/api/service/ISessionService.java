package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.util.List;

public interface ISessionService {

    @Nullable
    SessionDTO open(@Nullable String login, @Nullable String password);

    @Nullable
    SessionDTO sign(@Nullable SessionDTO sessionDTODTO);

    @Nullable
    UserDTO getUser(@Nullable SessionDTO sessionDTO);

    @Nullable
    String getUserId(@Nullable SessionDTO sessionDTO);

    @NotNull
    List<SessionDTO> getListSession(@Nullable SessionDTO sessionDTO);

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    void close(@Nullable SessionDTO sessionDTO) throws Exception;

    void closeAll(@Nullable SessionDTO sessionDTO);

    void validate (@Nullable SessionDTO sessionDTO);

    void validate (@Nullable SessionDTO sessionDTO, Role role);

    void signOutByLogin(@Nullable String login);

    void singOutByUserId(@Nullable String userId);
}
