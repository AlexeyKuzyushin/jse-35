package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.List;

@Repository
public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public @NotNull Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Project e", Long.class).getSingleResult();
    }

    @NotNull
    @Override
    public List<Project> findAll(final @NotNull String userId) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @NotNull List<Project> findAll() {
        return entityManager.createQuery("SELECT e FROM Project e", Project.class).getResultList();
    }

    @Nullable
    @Override
    public Project findById(final @NotNull String userId, final @NotNull String id) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.id = :id",
                Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public Project findByName(final @NotNull String userId, final @NotNull String name) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.name = :name",
                Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult();
    }

    public void clear() {
        entityManager.createQuery("DELETE FROM Project").executeUpdate();
    }

    @Override
    public void removeAll(final @NotNull String userId) {
        entityManager.createQuery("DELETE FROM Project e WHERE e.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeById(final @NotNull String userId, final @NotNull String id) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return;
        entityManager.remove(project);
    }

    @Override
    public void removeByName(final @NotNull String userId, final @NotNull String name) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return;
        entityManager.remove(project);
    }
}
