package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ITaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.List;

@Repository
public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public @NotNull Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Task e", Long.class).getSingleResult();
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("SELECT e FROM Task e", Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(final @NotNull String userId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.userId = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(final @NotNull String userId, final @NotNull String projectId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.projectId = :projectId AND e.userId = :userId",
                Task.class)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findById(final @NotNull String userId, final @NotNull String id) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.userId = :userId AND e.id = :id",
                Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public Task findByName(final @NotNull String userId, final @NotNull String name) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.userId = :userId AND e.name = :name",
                Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult();
    }

    public void clear() {
        entityManager.createQuery("DELETE FROM Task").executeUpdate();
    }

    @Override
    public void removeAll(final @NotNull String userId) {
        entityManager.createQuery("DELETE FROM Task e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeById(final @NotNull String userId, final @NotNull String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return;
        entityManager.remove(task);
    }

    @Override
    public void removeByName(final @NotNull String userId, final @NotNull String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return;
        entityManager.remove(task);
    }

    @Override
    public void removeAllByProjectId(final @NotNull String projectId) {
        entityManager.createQuery("DELETE FROM Task e WHERE e.projectId = :projectId")
                .setParameter("projectId", projectId).executeUpdate();
    }
}