package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ISessionRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import java.util.List;

@Repository
public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public @NotNull Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Session e", Long.class).getSingleResult();
    }

    @Override
    public @NotNull List<Session> findAll(final @NotNull String userId) {
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @NotNull List<Session> findAll() {
        return entityManager.createQuery("SELECT e FROM Session e", Session.class).getResultList();
    }

    @Override
    public @Nullable Session findById(final @NotNull String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Session").executeUpdate();
    }

    @Override
    @Nullable
    public Session findByUserId(final @NotNull String userId) {
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void removeAll(final @NotNull String userId) {
        entityManager.createQuery("DELETE FROM Session e WHERE e.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeById(final @NotNull String id) {
        @Nullable final Session session = findById(id);
        if (session == null) return;
        entityManager.remove(session);
    }

    @Override
    public void removeByUserId(final @NotNull String userId) {
        @Nullable final Session session = findByUserId(userId);
        if (session == null) return;
        entityManager.remove(session);
    }

    @Override
    public boolean contains(final @NotNull String id) {
        final Session session = findById(id);
        return findAll().contains(session);
    }

    public void remove(final @NotNull User user) {
        entityManager.remove(user);
    }

    public void persist(final @NotNull User user) {
        entityManager.persist(user);
    }

    public void merge(@Nullable User user) {
        entityManager.merge(user);
    }
}
