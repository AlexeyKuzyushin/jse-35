package ru.rencredit.jschool.kuzyushin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO extends AbstractEntityDTO {

    public static final long serialVersionUID = 1;

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

    @Nullable
    public static UserDTO toDTO(@Nullable final User user) {
        if (user == null) return null;
        return new UserDTO(user);
    }

    @NotNull
    public static List<UserDTO> toDTO(@Nullable final List<User> users) {
        if (users == null || users.isEmpty()) return Collections.emptyList();
        @NotNull final List<UserDTO> result = new ArrayList<>();
        for (@Nullable final User user: users) {
            if (user == null) continue;
            result.add(new UserDTO(user));
        }
        return result;
    }

    public UserDTO(@Nullable final User user) {
        if (user == null) return;
        id = user.getId();
        login = user.getLogin();
        passwordHash = user.getPasswordHash();
        email = user.getEmail();
        firstName = user.getFirstName();
        middleName = user.getMiddleName();
        lastName = user.getLastName();
        role = user.getRole();
        locked = user.getLocked();
    }
}
