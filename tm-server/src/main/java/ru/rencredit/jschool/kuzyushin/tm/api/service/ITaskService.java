package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService {

    @Nullable
    Long count();

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId, @Nullable String projectId);

    void create(@Nullable String userId, @Nullable String projectId, @Nullable String name, @Nullable String description);

    @Nullable
    Task findById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task findByName(@Nullable String userId, @Nullable String name);

    void clear();

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByName(@Nullable String userId, @Nullable String name);

    void removeAllByUserId(@Nullable String userId);

    void removeAllByProjectId(@Nullable String projectId);

    Task updateById(@Nullable String userId, @Nullable String id,
                       @Nullable String name, @Nullable String description);

    void updateStartDate(@Nullable String userId, @Nullable String id, @Nullable Date date);

    void updateFinishDate(@Nullable String userId, @Nullable String id, @Nullable Date date);

    void load(@Nullable List<TaskDTO> tasks);
}
