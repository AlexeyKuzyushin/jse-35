package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ITaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.*;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.*;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectStartDateException;

import java.util.Date;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskRepository taskRepository;

    @Autowired
    public TaskService(
            @NotNull final IUserService userService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskRepository taskRepository
    ) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    public Long count() {
        return taskRepository.count();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return TaskDTO.toDTO(taskRepository.findAll());
    }

    @NotNull
    @Override
    public  List<TaskDTO> findAll(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return TaskDTO.toDTO(taskRepository.findAll(userId));
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(final @Nullable String userId, final @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return TaskDTO.toDTO(taskRepository.findAll(userId));
    }

    @Override
    @Transactional
    public void create(final @Nullable String userId, final @Nullable String projectId,
                       final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(userService.findById(userId));
        task.setProject(projectService.findById(userId, projectId));
        taskRepository.persist(task);
    }

    @Nullable
    @Override
    public Task findById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(userId, id);
    }

    @Nullable
    @Override
    public Task findByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.clear();
    }

    @Override
    @Transactional
    public void removeAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.removeAll(userId);
    }

    @Override
    @Transactional
    public void removeById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.removeById(userId, id);
    }

    @Override
    @Transactional
    public void removeByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.removeByName(userId, name);
    }

    @Override
    @Transactional
    public void removeAllByProjectId(@Nullable String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(projectId);
    }

    @Nullable
    @Override
    @Transactional
    public Task updateById(
            final @Nullable String userId, final @Nullable String id,
            final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable Task task = findById(userId, id);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @Transactional
    public void updateStartDate(final @Nullable String userId, final @Nullable String id, final @Nullable Date date) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Task task = findById(userId, id);
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException(date);
        task.setStartDate(date);
        taskRepository.merge(task);
    }

    @Override
    @Transactional
    public void updateFinishDate(final @Nullable String userId, final @Nullable String id, final @Nullable Date date) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Task task = findById(userId, id);
        if (task.getStartDate() == null) throw new IncorrectStartDateException();
        if (task.getStartDate().after(date)) throw new IncorrectStartDateException(date);
        task.setFinishDate(date);
        taskRepository.merge(task);

    }

    @Override
    @Transactional
    public void load(final @Nullable List<TaskDTO> tasks) {
        if (tasks == null) return;
        taskRepository.clear();
        for (final TaskDTO taskDTO: tasks){
            @NotNull final Task task = new Task();
            task.setName(taskDTO.getName());
            task.setDescription(taskDTO.getDescription());
            task.setUser(userService.findById(taskDTO.getUserId()));
            task.setProject(projectService.findById(taskDTO.getUserId(), taskDTO.getProjectId()));
            task.setId(taskDTO.getId());
            task.setStartDate(taskDTO.getStartDate());
            task.setFinishDate(taskDTO.getFinishDate());
            task.setCreationTime(taskDTO.getCreationDate());
            taskRepository.persist(task);
        }
    }
}
