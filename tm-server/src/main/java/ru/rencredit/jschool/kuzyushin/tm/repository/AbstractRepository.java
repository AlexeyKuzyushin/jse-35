package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractRepository <E extends AbstractEntity> {

    @PersistenceContext
    protected EntityManager entityManager;

    public void remove(@NotNull E e) {
        entityManager.remove(e);
    }

    public void persist(@NotNull final E e) {
        entityManager.persist(e);
    }

    public void merge(@NotNull final E e) {
        entityManager.merge(e);
    }
}
