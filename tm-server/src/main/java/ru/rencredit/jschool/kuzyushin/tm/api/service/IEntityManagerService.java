package ru.rencredit.jschool.kuzyushin.tm.api.service;

import javax.persistence.EntityManager;

public interface IEntityManagerService {

    EntityManager getEntityManager();
}
