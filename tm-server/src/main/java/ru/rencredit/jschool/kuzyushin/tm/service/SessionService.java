package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ISessionRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.*;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.*;
import ru.rencredit.jschool.kuzyushin.tm.exception.user.AccessDeniedException;
import ru.rencredit.jschool.kuzyushin.tm.util.HashUtil;
import ru.rencredit.jschool.kuzyushin.tm.util.SignatureUtil;

import java.util.List;

@Service
public class SessionService implements ISessionService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionRepository sessionRepository;

    @Autowired
    public SessionService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService,
            @NotNull final ISessionRepository sessionRepository
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionRepository = sessionRepository;
    }

    @Override
    @Nullable
    @Transactional
    public SessionDTO open(final @Nullable String login, final @Nullable String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final User user = userService.findByLogin(login);
        if (user == null) return null;
        final Session session = new Session();
        session.setUser(user);
        session.setTimestamp(System.currentTimeMillis());
        final String salt = propertyService.getSessionSalt();
        final Integer cycle = propertyService.getSessionCycle();
        session.setSignature(SignatureUtil.sign(SessionDTO.toDTO(session),salt, cycle));
        sessionRepository.persist(session);
        return SessionDTO.toDTO(session);
    }

    @Override
    @Nullable
    public SessionDTO sign(final @Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        sessionDTO.setSignature(null);
        final String salt = propertyService.getSessionSalt();
        final Integer cycle = propertyService.getSessionCycle();
        final String signature = SignatureUtil.sign(sessionDTO, salt, cycle);
        sessionDTO.setSignature(signature);
        return sessionDTO;
    }

    @Override
    @Nullable
    public UserDTO getUser(final @Nullable SessionDTO sessionDTO) {
        final String userId = getUserId(sessionDTO);
        return UserDTO.toDTO(userService.findById(userId));
    }

    @Override
    @Nullable
    public String getUserId(final @Nullable SessionDTO sessionDTO) {
        validate(sessionDTO);
        return sessionDTO.getUserId();
    }

    @NotNull
    @Override
    public  List<SessionDTO> getListSession(final @Nullable SessionDTO sessionDTO) {
        validate(sessionDTO);
        @NotNull final List<SessionDTO> sessionsDTO = SessionDTO.toDTO(sessionRepository.findAll(sessionDTO.getUserId()));
        return sessionsDTO;
    }

    @Override
    public boolean checkDataAccess(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @Transactional
    public void close(final @Nullable SessionDTO sessionDTO){
        validate(sessionDTO);
        sessionRepository.removeById(sessionDTO.getId());
    }

    @Override
    @Transactional
    public void closeAll(@Nullable SessionDTO sessionDTO) {
        validate(sessionDTO);
        sessionRepository.removeAll(sessionDTO.getUserId());
    }

    @Override
    @Transactional(readOnly = true)
    public void validate(@Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) throw new AccessDeniedException();
        if (sessionDTO.getSignature() == null ||
                sessionDTO.getSignature().isEmpty()) throw new AccessDeniedException();
        if (sessionDTO.getUserId() == null ||
                sessionDTO.getUserId().isEmpty()) throw new EmptyUserIdException();
        if (sessionDTO.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = sessionDTO.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = sessionDTO.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        final boolean isContain = sessionRepository.contains(sessionDTO.getId());
        if (!isContain) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable SessionDTO sessionDTO, Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(sessionDTO);
        final String userId = sessionDTO.getUserId();
        final User user = userService.findById(userId);
        if (user == null) throw new EmptyUserException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @Transactional
    public void signOutByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        @Nullable final UserDTO userDTO = UserDTO.toDTO(userService.findByLogin(login));
        if (userDTO == null) throw new AccessDeniedException();
        @NotNull final String userId = userDTO.getId();
        sessionRepository.removeByUserId(userId);
    }

    @Override
    @Transactional
    public void singOutByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        sessionRepository.removeByUserId(userId);
    }
}
