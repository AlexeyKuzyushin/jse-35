package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.AbstractEntity;


public interface IRepository <E extends AbstractEntity> {

    void remove (@NotNull E e);

    void persist(@Nullable E e);

    void merge(@Nullable E e);
}
