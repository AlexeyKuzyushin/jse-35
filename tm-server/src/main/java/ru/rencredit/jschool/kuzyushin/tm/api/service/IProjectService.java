package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.Date;
import java.util.List;

public interface IProjectService {

    @Nullable
    Long count();

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAllByUserId(@Nullable String userId);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project findById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findByName(@Nullable String userId, @Nullable String name);

    void clear();

    void removeAllByUserId(@Nullable String userId);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByName(@Nullable String userId, @Nullable String name);

    Project updateById(@Nullable String userId, @Nullable String id,
                       @Nullable String name, @Nullable String description);

    void updateStartDate(@Nullable String userId, @Nullable String id, @Nullable Date date);

    void updateFinishDate(@Nullable String userId, @Nullable String id, @Nullable Date date);

    void load(@Nullable List<ProjectDTO> tasks);
}
