package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IUserRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import java.util.List;

@Repository
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public Long count() {
            return entityManager.createQuery("SELECT COUNT(e) FROM User e", Long.class).getSingleResult();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT e FROM User e", User.class).getResultList();
    }

    @Nullable
    @Override
    public User findById(final @NotNull String id) {
        final List<User> users =
                entityManager.createQuery("SELECT e FROM User e WHERE e.id = :id", User.class)
                        .setParameter("id", id)
                        .setMaxResults(1)
                        .getResultList();
        return users.get(0);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        final List<User> users = entityManager.createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .getResultList();
        return users.get(0);
    }


    @Override
    public void removeById(final @NotNull String id) {
        @Nullable final User user = findById(id);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public void removeByLogin(final @NotNull String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return;
        entityManager.remove(user);
    }
}
