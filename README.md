# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO
**NAME**: ALEXEY KUZYUSHIN

**E-MAIL**: alexeykuzyushin@yandex.ru

# SOFTWARE

- [JDK 1.8](https://openjdk.java.net/install/)
- [Apache Maven 3.6.3](https://maven.apache.org/download.cgi)
- [IntelliJ IDEA Community Edition](https://www.jetbrains.com/ru-ru/idea/download/#section=linux)
- [Ubuntu 20.10](https://releases.ubuntu.com/20.10/)

# HARDWARE

- CPU: Intel i5-3337U or highter
- RAM: 8 Gb DD3-1600MHz or highter

# PROGRAM BUILD
```bash
mvn clean install
```

# PROGRAM RUN
``` bash
java -jar ./task-manager-1.0.0.jar
```

# DOCKER

## DOCKER RUN
``` bash
docker-compose up -d
```

## DOCKER SHUTDOWN
``` bash
docker-compose down
```
