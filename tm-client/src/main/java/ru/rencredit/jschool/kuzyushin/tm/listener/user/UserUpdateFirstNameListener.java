package ru.rencredit.jschool.kuzyushin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.UserEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class UserUpdateFirstNameListener extends AbstractListener {

    @NotNull
    private final UserEndpoint userEndpoint;

    @Autowired
    public UserUpdateFirstNameListener(
            final @NotNull UserEndpoint userEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "user-update-first-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user first name";
    }

    @Override
    @EventListener(condition = "@userUpdateFirstNameListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[UPDATE USER FIRST NAME]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        System.out.println("ENTER FIRST NAME:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        userEndpoint.updateUserFirstName(sessionDTO, firstName);
        System.out.println("[OK]");
    }
}
