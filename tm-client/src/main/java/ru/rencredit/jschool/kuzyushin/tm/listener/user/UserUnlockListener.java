package ru.rencredit.jschool.kuzyushin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.UserEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class UserUnlockListener extends AbstractListener {

    @NotNull
    private final UserEndpoint userEndpoint;

    @Autowired
    public UserUnlockListener(
            final @NotNull UserEndpoint userEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "user-unlock";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user";
    }

    @Override
    @EventListener(condition = "@userUnlockListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[UNLOCK USER]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        System.out.println("[ENTER LOGIN:]");
        @Nullable final String login = TerminalUtil.nextLine();
        userEndpoint.unlockUserByLogin(sessionDTO, login);
        System.out.println("[OK]");
    }
}
