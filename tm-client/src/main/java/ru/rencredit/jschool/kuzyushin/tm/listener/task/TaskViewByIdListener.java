package ru.rencredit.jschool.kuzyushin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class TaskViewByIdListener extends AbstractListener {

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @Autowired
    public TaskViewByIdListener(
            final @NotNull TaskEndpoint taskEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by id";
    }

    @Override
    @EventListener(condition = "@taskViewByIdListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        @Nullable final TaskDTO taskDTO = taskEndpoint.findTaskById(sessionDTO, id);
        if (taskDTO == null) return;
        System.out.println("ID: " + taskDTO.getId());
        System.out.println("NAME: " + taskDTO.getName());
        System.out.println("DESCRIPTION: " + taskDTO.getDescription());
        System.out.println("[OK]");
    }
}
