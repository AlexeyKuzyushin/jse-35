package ru.rencredit.jschool.kuzyushin.tm.listener.data.xml;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.DataEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

@Component
public class DataXmlLoadListener extends AbstractListener {

    @NotNull
    private final DataEndpoint dataEndpoint;

    @Autowired
    public DataXmlLoadListener(
            final @NotNull DataEndpoint dataEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.dataEndpoint = dataEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from xml file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlLoadListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[DATA XML LOAD]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        dataEndpoint.loadDataXml(sessionDTO);
        System.out.println("[OK]");
    }
}