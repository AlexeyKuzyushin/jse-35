package ru.rencredit.jschool.kuzyushin.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.*;

@Configuration
@ComponentScan(basePackages = "ru.rencredit.jschool.kuzyushin.tm")
public class ClientConfiguration {

    @Bean
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    public SessionEndpoint sessionEndpoint(
            @NotNull @Autowired final SessionEndpointService sessionEndpointService
    ) {
        return sessionEndpointService().getSessionEndpointPort();
    }

    @Bean
    public DataEndpointService dataEndpointService() {
        return new DataEndpointService();
    }

    @Bean
    public DataEndpoint dataEndpoint(
            @NotNull @Autowired final DataEndpointService dataEndpointService
    ) {
        return dataEndpointService().getDataEndpointPort();
    }

    @Bean
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    public TaskEndpoint taskEndpoint(
            @NotNull @Autowired final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService().getTaskEndpointPort();
    }

    @Bean
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    public ProjectEndpoint projectEndpoint(
            @NotNull @Autowired final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService().getProjectEndpointPort();
    }

    @Bean
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    public UserEndpoint userEndpoint(
            @NotNull @Autowired final UserEndpointService userEndpointService
    ) {
        return userEndpointService().getUserEndpointPort();
    }
}
