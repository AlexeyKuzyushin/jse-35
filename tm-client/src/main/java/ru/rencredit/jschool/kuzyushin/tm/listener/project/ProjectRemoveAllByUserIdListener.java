package ru.rencredit.jschool.kuzyushin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

@Component
public final class ProjectRemoveAllByUserIdListener extends AbstractListener {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @Autowired
    public ProjectRemoveAllByUserIdListener(
            final @NotNull ProjectEndpoint projectEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-all-by-userId";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects by user";
    }

    @Override
    @EventListener(condition = "@projectRemoveAllByUserIdListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[REMOVE All PROJECT]");
            @Nullable SessionDTO sessionDTO = sessionService.getCurrentSession();
            projectEndpoint.removeAllProjectsByUserId(sessionDTO);
            System.out.println("[OK]");
    }
}
